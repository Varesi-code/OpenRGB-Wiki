# Galax GPUs

## **Registers**

| Register address | Register description |
| ---------------- | -------------------- |
| 0x02             | Red                  |
| 0x03             | Green                |
| 0x04             | Blue                 |
| 0x05             | Mode 1               |
| 0x06             | Mode 2               |

## **Modes**

| Mode Name       | Mode 1 Value | Mode 2 Value |
| --------------- | ------------ | ------------ |
| Static          | 0x00         | 0x01         |
| Breathing       | 0x04         | 0x00         |
| Rainbow         | 0x84         | 0x02         |
| Cycle Breathing | 0x84         | 0x40         |
