# Sapphire Nitro Glow v1

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `1002:67DF` | `174B:E347` | RX 470/480 Nitro+ |
| `1002:67DF` | `1DA2:E366` | RX 570/580/590 Nitro+ |
| `1002:67DF` | `1DA2:E399` | RX 570/580/590 Nitro+ |
| `1002:6FDF` | `1DA2:E366` | RX 580 Nitro+ (2048SP) |
| `1002:687F` | `1DA2:E37F` | RX Vega 56/64 Nitro+ |
| `1002:731F` | `1DA2:E409` | RX 5700 (XT) Nitro+ |
| `1002:731F` | `1DA2:E410` | RX 5700 XT Nitro+ |
| `1002:731F` | `1DA2:426E` | RX 5700 XT Nitro+ |
| `1002:7340` | `1DA2:E423` | RX 5500 XT Nitro+ |
| `1002:73BF` | `1DA2:438E` | RX 6800 XT Nitro+ SE |
| `1002:73BF` | `1DA2:E438` | RX 6800 XT/6900 XT Nitro+ |
| `1002:73BF` | `1DA2:E439` | RX 6800 Nitro+ |
| `1002:73DF` | `1DA2:E445` | RX 6700 XT Nitro+ |
| `1002:73FF` | `1DA2:E448` | RX 6600 XT Nitro+ |
| `1002:73AF` | `1DA2:F440` | RX 6900 XT Toxic |
