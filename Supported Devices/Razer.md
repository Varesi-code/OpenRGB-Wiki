# Razer

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1532` | `0241` | Razer Blackwidow 2019 |
| `1532` | `0203` | Razer Blackwidow Chroma |
| `1532` | `0209` | Razer Blackwidow Chroma Tournament Edition |
| `1532` | `0221` | Razer Blackwidow Chroma V2 |
| `1532` | `0228` | Razer Blackwidow Elite |
| `1532` | `0211` | Razer Blackwidow Overwatch |
| `1532` | `024E` | Razer Blackwidow V3 |
| `1532` | `025A` | Razer Blackwidow V3 Pro (Wired) |
| `1532` | `025C` | Razer Blackwidow V3 Pro (Wireless) |
| `1532` | `0A24` | Razer Blackwidow V3 TKL |
| `1532` | `0258` | Razer Blackwidow V3 Mini (Wired) |
| `1532` | `0271` | Razer Blackwidow V3 Mini (Wireless) |
| `1532` | `0216` | Razer Blackwidow X Chroma |
| `1532` | `021A` | Razer Blackwidow X Chroma Tournament Edition |
| `1532` | `022A` | Razer Cynosa Chroma |
| `1532` | `025E` | Razer Cynosa Chroma V2 |
| `1532` | `023F` | Razer Cynosa Lite |
| `1532` | `0204` | Razer Deathstalker Chroma |
| `1532` | `0227` | Razer Huntsman |
| `1532` | `0226` | Razer Huntsman Elite |
| `1532` | `0257` | Razer Huntsman Mini |
| `1532` | `0243` | Razer Huntsman Tournament Edition |
| `1532` | `0266` | Razer Huntsman V2 Analog |
| `1532` | `026B` | Razer Huntsman V2 TKL |
| `1532` | `026C` | Razer Huntsman V2 |
| `1532` | `021E` | Razer Ornata Chroma |
| `1532` | `025D` | Razer Ornata Chroma V2 |
| `1532` | `020F` | Razer Blade (2016) |
| `1532` | `0224` | Razer Blade (Late 2016) |
| `1532` | `0270` | Razer Blade 14 (2021) |
| `1532` | `0233` | Razer Blade 15 (2018 Advanced) |
| `1532` | `023B` | Razer Blade 15 (2018 Base) |
| `1532` | `0240` | Razer Blade 15 (2018 Mercury) |
| `1532` | `023A` | Razer Blade 15 (2019 Advanced) |
| `1532` | `0246` | Razer Blade 15 (2019 Base) |
| `1532` | `0245` | Razer Blade 15 (2019 Mercury) |
| `1532` | `024D` | Razer Blade 15 (2019 Studio) |
| `1532` | `0253` | Razer Blade 15 (2020 Advanced) |
| `1532` | `0255` | Razer Blade 15 (2020 Base) |
| `1532` | `0268` | Razer Blade 15 (Late 2020) |
| `1532` | `026D` | Razer Blade 15 (2021 Advanced) |
| `1532` | `026F` | Razer Blade 15 (2021 Base) |
| `1532` | `0210` | Razer Blade Pro (2016) |
| `1532` | `0225` | Razer Blade Pro (2017) |
| `1532` | `022F` | Razer Blade Pro (2017 FullHD) |
| `1532` | `0234` | Razer Blade Pro (2019) |
| `1532` | `024C` | Razer Blade Pro (Late 2019) |
| `1532` | `0256` | Razer Blade Pro 17 (2020) |
| `1532` | `0279` | Razer Blade Pro 17 (2021) |
| `1532` | `0205` | Razer Blade Stealth (2016) |
| `1532` | `0220` | Razer Blade Stealth (Late 2016) |
| `1532` | `022D` | Razer Blade Stealth (2017) |
| `1532` | `0232` | Razer Blade Stealth (Late 2017) |
| `1532` | `0239` | Razer Blade Stealth (2019) |
| `1532` | `024A` | Razer Blade Stealth (Late 2019) |
| `1532` | `0252` | Razer Blade Stealth (2020) |
| `1532` | `0259` | Razer Blade Stealth (Late 2020) |
| `1532` | `026A` | Razer Book 13 (2020) |
| `1532` | `006A` | Razer Abyssus Elite D.Va Edition |
| `1532` | `006B` | Razer Abyssus Essential |
| `1532` | `0064` | Razer Basilisk |
| `1532` | `0065` | Razer Basilisk Essential |
| `1532` | `0086` | Razer Basilisk Ultimate (Wired) |
| `1532` | `0088` | Razer Basilisk Ultimate (Wireless) |
| `1532` | `0085` | Razer Basilisk V2 |
| `1532` | `0099` | Razer Basilisk V3 |
| `1532` | `0043` | Razer Deathadder Chroma |
| `1532` | `005C` | Razer Deathadder Elite |
| `1532` | `006E` | Razer Deathadder Essential |
| `1532` | `0071` | Razer Deathadder Essential White Edition |
| `1532` | `0084` | Razer Deathadder V2 |
| `1532` | `008C` | Razer Deathadder V2 Mini |
| `1532` | `007C` | Razer Deathadder V2 Pro (Wired) |
| `1532` | `007D` | Razer Deathadder V2 Pro (Wireless) |
| `1532` | `004C` | Razer Diamondback |
| `1532` | `0059` | Razer Lancehead 2017 (Wired) |
| `1532` | `005A` | Razer Lancehead 2017 (Wireless) |
| `1532` | `0070` | Razer Lancehead 2019 (Wired) |
| `1532` | `006F` | Razer Lancehead 2019 (Wireless) |
| `1532` | `0060` | Razer Lancehead Tournament Edition |
| `1532` | `0024` | Razer Mamba 2012 (Wired) |
| `1532` | `0025` | Razer Mamba 2012 (Wireless) |
| `1532` | `0044` | Razer Mamba 2015 (Wired) |
| `1532` | `0045` | Razer Mamba 2015 (Wireless) |
| `1532` | `0073` | Razer Mamba 2018 (Wired) |
| `1532` | `0072` | Razer Mamba 2018 (Wireless) |
| `1532` | `006C` | Razer Mamba Elite |
| `1532` | `0046` | Razer Mamba Tournament Edition |
| `1532` | `0053` | Razer Naga Chroma |
| `1532` | `003E` | Razer Naga Epic Chroma |
| `1532` | `008D` | Razer Naga Left Handed |
| `1532` | `0050` | Razer Naga Hex V2 |
| `1532` | `0067` | Razer Naga Trinity |
| `1532` | `008F` | Razer Naga Pro (Wired) |
| `1532` | `0090` | Razer Naga Pro (Wireless) |
| `1532` | `0078` | Razer Viper |
| `1532` | `0091` | Razer Viper 8kHz |
| `1532` | `008A` | Razer Viper Mini |
| `1532` | `007A` | Razer Viper Ultimate (Wired) |
| `1532` | `007B` | Razer Viper Ultimate (Wireless) |
| `1532` | `0207` | Razer Orbweaver Chroma |
| `1532` | `0208` | Razer Tartarus Chroma |
| `1532` | `022B` | Razer Tartarus V2 |
| `1532` | `0F19` | Razer Kraken Kitty Edition |
| `1532` | `0F21` | Razer Kraken Kitty Black Edition |
| `1532` | `0F03` | Razer Tiamat 7.1 V2 |
| `1532` | `0C00` | Razer Firefly |
| `1532` | `0C04` | Razer Firefly V2 |
| `1532` | `0068` | Razer Firefly Hyperflux |
| `1532` | `0C01` | Razer Goliathus |
| `1532` | `0C02` | Razer Goliathus Extended |
| `1532` | `0F08` | Razer Base Station Chroma |
| `1532` | `0F20` | Razer Base Station V2 Chroma |
| `1532` | `0F26` | Razer Charging Pad Chroma |
| `1532` | `0F09` | Razer Chroma HDK |
| `1532` | `0F07` | Razer Chroma Mug Holder |
| `1532` | `0F0E` | Razer Chroma PC Case Lighting Kit |
| `1532` | `0215` | Razer Core |
| `1532` | `0F1A` | Razer Core X |
| `1532` | `0F2B` | Razer Laptop Stand Chroma V2 |
| `1532` | `0F1D` | Razer Mouse Bungee V3 Chroma |
| `1532` | `007E` | Razer Mouse Dock Chroma |
| `1532` | `0F13` | Lian Li O11 Dynamic - Razer Edition |
| `1532` | `0F1B` | Razer Seiren Emote |
| `1532` | `0517` | Razer Nommo Chroma |
| `1532` | `0518` | Razer Nommo Pro |
