# Logitech G910

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `C32B` | Logitech G910 Orion Spark |
| `046D` | `C335` | Logitech G910 Orion Spectrum |
