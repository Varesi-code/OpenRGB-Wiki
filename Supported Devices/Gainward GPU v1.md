# Gainward GPU v1

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1B80` | `10B0:1B80` | Gainward GTX 1080 Phoenix |
| `10DE:1B06` | `10B0:1B06` | Gainward GTX 1080 Ti Phoenix |
| `10DE:1E84` | `10B0:1E84` | Gainward RTX 2070 Super Phantom |
| `10DE:2484` | `10B0:2484` | Gainward RTX 3070 Phoenix |
