# Coolermaster ARGB

 The Coolermaster ARGB device supports `Direct` mode from
firmware 0028 onwards. Check the serial number for the date
"A202105291658" or newer.

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `1011` | Cooler Master ARGB |
