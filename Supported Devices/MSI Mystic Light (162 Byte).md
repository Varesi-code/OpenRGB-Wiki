# MSI Mystic Light (162 Byte)

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1462` | `1720` | MSI Mystic Light MS_1720 |
| `1462` | `7B12` | MSI Mystic Light MS_7B12 |
| `1462` | `7B17` | MSI Mystic Light MS_7B17 |
| `1462` | `7B18` | MSI Mystic Light MS_7B18 |
| `1462` | `7B85` | MSI Mystic Light MS_7B85 |
