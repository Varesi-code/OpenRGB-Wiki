# Galax GPU

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1F02` | `10DE:12FE` | KFA2 RTX 2070 EX |
| `10DE:1E84` | `10DE:139F` | GALAX RTX 2070 Super EX Gamer Black |
| `10DE:1E82` | `10DE:12B0` | KFA2 RTX 2080 EX OC |
