# Steel Series Rival

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1038` | `1702` | SteelSeries Rival 100 |
| `1038` | `170c` | SteelSeries Rival 100 DotA 2 Edition |
| `1038` | `1814` | SteelSeries Rival 105 |
| `1038` | `1816` | SteelSeries Rival 106 |
| `1038` | `1729` | SteelSeries Rival 110 |
| `1038` | `1710` | SteelSeries Rival 300 |
| `1038` | `1714` | Acer Predator Gaming Mouse (Rival 300) |
| `1038` | `1394` | SteelSeries Rival 300 CS:GO Fade Edition |
| `1038` | `1716` | SteelSeries Rival 300 CS:GO Fade Edition (stm32) |
| `1038` | `171a` | SteelSeries Rival 300 CS:GO Hyperbeast Edition |
| `1038` | `1392` | SteelSeries Rival 300 Dota 2 Edition |
| `1038` | `1718` | SteelSeries Rival 300 HP Omen Edition |
| `1038` | `1710` | SteelSeries Rival 300 Black Ops Edition |
| `1038` | `1724` | SteelSeries Rival 600 |
| `1038` | `172E` | SteelSeries Rival 600 Dota 2 Edition |
| `1038` | `172B` | SteelSeries Rival 650 |
| `1038` | `1726` | SteelSeries Rival 650 Wireless |
| `1038` | `1700` | SteelSeries Rival 700 |
| `1038` | `1730` | SteelSeries Rival 710 |
