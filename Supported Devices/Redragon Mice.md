# Redragon Mice

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `04D9` | `FC30` | Redragon M711 Cobra |
| `04D9` | `FC39` | Redragon M715 Dagger |
| `04D9` | `FC3A` | Redragon M716 Inquisitor |
| `04D9` | `FC4D` | Redragon M908 Impact |
| `04D9` | `FC38` | Redragon M602 Griffin |
