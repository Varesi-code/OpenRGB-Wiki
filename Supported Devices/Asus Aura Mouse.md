# Asus Aura Mouse

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `18DD` | ASUS ROG Gladius II Core |
| `0B05` | `1845` | ASUS ROG Gladius II |
| `0B05` | `1877` | ASUS ROG Gladius II Origin |
| `0B05` | `18CD` | ASUS ROG Gladius II Origin PNK LTD |
| `0B05` | `18B1` | ASUS ROG Gladius II Origin COD |
| `0B05` | `189E` | ASUS ROG Gladius II Wireless |
| `0B05` | `18A0` | ASUS ROG Gladius II Wireless |
| `0B05` | `197D` | ASUS ROG Gladius III Wireless USB |
| `0B05` | `197F` | ASUS ROG Gladius III Wireless 2.4Ghz |
| `0B05` | `1981` | ASUS ROG Gladius III Wireless Bluetooth |
| `0B05` | `18E5` | ASUS ROG Chakram (Wireless) |
| `0B05` | `18E3` | Asus ROG Chakram (Wired) |
| `0B05` | `1958` | Asus ROG Chakram Core |
| `0B05` | `1846` | ASUS ROG Pugio |
| `0B05` | `1906` | ASUS ROG Pugio II (Wired) |
| `0B05` | `1908` | ASUS ROG Pugio II (Wireless) |
| `0B05` | `1847` | ASUS ROG Strix Impact |
| `0B05` | `18E1` | ASUS ROG Strix Impact II |
| `0B05` | `189E` | ASUS ROG Strix Impact II Gundam |
| `0B05` | `1956` | ASUS ROG Strix Impact II Electro Punk |
| `0B05` | `19D2` | ASUS ROG Strix Impact II Moonlight White |
| `0B05` | `1947` | ASUS ROG Strix Impact II Wireless USB |
| `0B05` | `1949` | ASUS ROG Strix Impact II Wireless 2.4 Ghz |
| `0B05` | `195C` | ASUS ROG Keris |
| `0B05` | `195E` | ASUS ROG Keris Wireless USB |
| `0B05` | `1960` | ASUS ROG Keris Wireless 2.4Ghz |
| `0B05` | `1962` | ASUS ROG Keris Wireless Bluetooth |
| `0B05` | `1910` | ASUS TUF Gaming M3 |
| `0B05` | `1898` | ASUS TUF Gaming M5 |
