# HyperX Alloy Origins

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0951` | `16E6` | HyperX Alloy Origins Core |
| `0951` | `16E5` | HyperX Alloy Origins |
