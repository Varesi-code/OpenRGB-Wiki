# Das Keyboard

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `24F0` | `2037` | Das Keyboard Q4 RGB |
| `24F0` | `2020` | Das Keyboard Q5 RGB |
