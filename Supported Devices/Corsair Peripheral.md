# Corsair Peripheral

 
All controllers support `Direct` mode
Currently HW modes are implemented for the following devices:
* Corsair K70 RGB MK.2
* Corsair K70 RGB MK.2 Low Profile

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not fully implemented by controller (See device page for details)

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1B1C` | `1B3D` | Corsair K55 RGB |
| `1B1C` | `1B17` | Corsair K65 RGB |
| `1B1C` | `1B37` | Corsair K65 LUX RGB |
| `1B1C` | `1B39` | Corsair K65 RGB RAPIDFIRE |
| `1B1C` | `1B4F` | Corsair K68 RGB |
| `1B1C` | `1B13` | Corsair K70 RGB |
| `1B1C` | `1B33` | Corsair K70 LUX RGB |
| `1B1C` | `1B38` | Corsair K70 RGB RAPIDFIRE |
| `1B1C` | `1B49` | Corsair K70 RGB MK.2 |
| `1B1C` | `1B6B` | Corsair K70 RGB MK.2 SE |
| `1B1C` | `1B55` | Corsair K70 RGB MK.2 Low Profile |
| `1B1C` | `1B11` | Corsair K95 RGB |
| `1B1C` | `1B2D` | Corsair K95 RGB PLATINUM |
| `1B1C` | `1B20` | Corsair Strafe |
| `1B1C` | `1B44` | Corsair Strafe Red |
| `1B1C` | `1B48` | Corsair Strafe MK.2 |
| `1B1C` | `1B34` | Corsair Glaive RGB |
| `1B1C` | `1B74` | Corsair Glaive RGB PRO |
| `1B1C` | `1B3C` | Corsair Harpoon RGB |
| `1B1C` | `1B75` | Corsair Harpoon RGB PRO |
| `1B1C` | `1B5D` | Corsair Ironclaw RGB |
| `1B1C` | `1B12` | Corsair M65 |
| `1B1C` | `1B2E` | Corsair M65 PRO |
| `1B1C` | `1B5A` | Corsair M65 RGB Elite |
| `1B1C` | `1B5C` | Corsair Nightsword |
| `1B1C` | `1B1E` | Corsair Scimitar RGB |
| `1B1C` | `1B3E` | Corsair Scimitar PRO RGB |
| `1B1C` | `1B8B` | Corsair Scimitar Elite RGB |
| `1B1C` | `1B2F` | Corsair Sabre RGB |
| `1B1C` | `1B3B` | Corsair MM800 RGB Polaris |
| `1B1C` | `0A34` | Corsair ST100 RGB |
