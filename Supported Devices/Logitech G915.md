# Logitech G915

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `C541` | Logitech G915 Wireless RGB Mechanical Gaming Keyboard |
| `046D` | `C33E` | Logitech G915 Wireless RGB Mechanical Gaming Keyboard (Wired) |
| `046D` | `C545` | Logitech G915TKL Wireless RGB Mechanical Gaming Keyboard |
| `046D` | `C343` | Logitech G915TKL Wireless RGB Mechanical Gaming Keyboard (Wired) |
| `046D` | `C33E` | Logitech G915 Wireless RGB Mechanical Gaming Keyboard (Wired) |
| `046D` | `C343` | Logitech G915TKL Wireless RGB Mechanical Gaming Keyboard (Wired) |
