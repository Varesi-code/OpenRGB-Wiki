# Steel Series QCK Mat

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1038` | `150A` | SteelSeries QCK Prism Cloth Medium |
| `1038` | `150D` | SteelSeries QCK Prism Cloth XL |
| `1038` | `151E` | SteelSeries QCK Prism Cloth XL Destiny Ed. |
| `1038` | `1516` | SteelSeries QCK Prism Cloth 3XL |
| `1038` | `1518` | SteelSeries QCK Prism Cloth 4XL |
