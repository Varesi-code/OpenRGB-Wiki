# Coolermaster Masterkeys Keyboards

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `003B` | Cooler Master MasterKeys Pro L |
| `2516` | `0047` | Cooler Master MasterKeys Pro L White |
| `2516` | `003C` | Cooler Master MasterKeys Pro S |
| `2516` | `0067` | Cooler Master MK570 |
| `2516` | `0089` | Cooler Master SK630 |
| `2516` | `008D` | Cooler Master SK650 |
