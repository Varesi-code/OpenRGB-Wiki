# Coolermaster Master Mouse

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `0065` | Cooler Master MM530 |
| `2516` | `0101` | Cooler Master MM711 |
| `2516` | `0141` | Cooler Master MM720 |
| `2516` | `0109` | Cooler Master MP750 XL |
| `2516` | `0107` | Cooler Master MP750 Large |
| `2516` | `0105` | Cooler Master MP750 Medium |
