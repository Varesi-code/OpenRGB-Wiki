# Steel Series APEX

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1038` | `161A` | SteelSeries Apex 3 |
| `1038` | `161C` | SteelSeries Apex 5 |
| `1038` | `1612` | SteelSeries Apex 7 |
| `1038` | `1618` | SteelSeries Apex 7 TKL |
| `1038` | `1610` | SteelSeries Apex Pro |
| `1038` | `1614` | SteelSeries Apex Pro TKL |
| `1038` | `0616` | SteelSeries Apex M750 |
| `1038` | `1202` | SteelSeries Apex (OG)/Apex Fnatic |
| `1038` | `1206` | SteelSeries Apex 350 |
| `1038` | `1618` | SteelSeries Apex 7 TKL |
| `1038` | `1614` | SteelSeries Apex Pro TKL |
| `1038` | `0616` | SteelSeries Apex M750 |
