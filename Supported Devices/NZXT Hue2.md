# NZXT Hue2

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1E71` | `2001` | NZXT Hue 2 |
| `1E71` | `2002` | NZXT Hue 2 Ambient |
| `1E71` | `2005` | NZXT Hue 2 Motherboard |
| `1E71` | `2002` | NZXT Hue 2 Ambient |
| `1E71` | `2005` | NZXT Hue 2 Motherboard |
| `1E71` | `2006` | NZXT Smart Device V2 |
| `1E71` | `200D` | NZXT Smart Device V2 |
| `1E71` | `200F` | NZXT Smart Device V2 |
| `1E71` | `2007` | NZXT Kraken X3 |
| `1E71` | `2009` | NZXT RGB & Fan Controller |
| `1E71` | `2010` | NZXT RGB & Fan Controller |
| `1E71` | `200E` | NZXT RGB & Fan Controller |
