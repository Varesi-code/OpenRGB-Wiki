# Sinowealth Mice

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `258A` | `0036` | Glorious Model O / O- |
| `258A` | `0033` | Glorious Model D / D- |
| `258A` | `0029` | Everest GT-100 RGB |
| `258A` | `1007` | ZET Fury Pro |
