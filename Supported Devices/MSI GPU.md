# MSI GPU

 

## Connection Type
 I2C

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1B81` | `1462:3306` | MSI GeForce GTX 1070 Gaming X |
| `10DE:2184` | `1462:3790` | MSI GeForce GTX 1660 Gaming X 6G |
| `10DE:2182` | `1462:375C` | MSI GeForce GTX 1660Ti Gaming 6G |
| `10DE:2182` | `1462:375A` | MSI GeForce GTX 1660Ti Gaming X 6G |
| `10DE:21C4` | `1462:C759` | MSI GeForce GTX 1660 Super Gaming 6G |
| `10DE:21C4` | `1462:C758` | MSI GeForce GTX 1660 Super Gaming X 6G |
| `10DE:1E89` | `1462:3752` | MSI GeForce RTX 2060 Gaming Z 6G |
| `10DE:1F08` | `1462:3752` | MSI GeForce RTX 2060 Gaming Z 6G |
| `10DE:1F08` | `1462:3754` | MSI GeForce RTX 2060 Gaming Z 6G |
| `10DE:1F06` | `1462:C752` | MSI GeForce RTX 2060 Super Gaming X |
| `10DE:1F06` | `1462:C754` | MSI GeForce RTX 2060 Super ARMOR OC |
| `10DE:1F07` | `1462:3732` | MSI GeForce RTX 2070 Gaming Z |
| `10DE:1F02` | `1462:3733` | MSI GeForce RTX 2070 Gaming |
| `10DE:1F02` | `1462:3734` | MSI GeForce RTX 2070 ARMOR |
| `10DE:1F07` | `1462:3734` | MSI GeForce RTX 2070 ARMOR OC |
| `10DE:1E84` | `1462:373F` | MSI GeForce RTX 2070 Super Gaming |
| `10DE:1E84` | `1462:C727` | MSI GeForce RTX 2070 Super Gaming Trio |
| `10DE:1E84` | `1462:373e` | MSI GeForce RTX 2070 Super Gaming X |
| `10DE:1E84` | `1462:C726` | MSI GeForce RTX 2070 Super Gaming X Trio |
| `10DE:1E84` | `1462:37B6` | MSI GeForce RTX 2070 Super Gaming Z Trio |
| `10DE:1E82` | `1462:372E` | MSI GeForce RTX 2080 Gaming Trio |
| `10DE:1E87` | `1462:3726` | MSI GeForce RTX 2080 Gaming X Trio |
| `10DE:1E87` | `1462:3728` | MSI GeForce RTX 2080 Sea Hawk EK X |
| `10DE:1E87` | `1462:3721` | MSI GeForce RTX 2080 Duke 8G OC |
| `10DE:1E81` | `1462:C724` | MSI GeForce RTX 2080 Super Gaming X Trio |
| `10DE:1E07` | `1462:3715` | MSI GeForce RTX 2080Ti Gaming X Trio |
| `10DE:1E07` | `1462:3716` | MSI GeForce RTX 2080Ti 11G Gaming X Trio |
| `10DE:1E07` | `1462:3717` | MSI GeForce RTX 2080Ti Sea Hawk EK X |
| `10DE:2504` | `1462:3903` | MSI GeForce RTX 3060 12G Gaming X Trio LHR |
| `10DE:2503` | `1462:3903` | MSI GeForce RTX 3060 12GB Gaming X Trio |
| `10DE:2486` | `1462:3903` | MSI GeForce RTX 3060 Ti 8GB Gaming X Trio |
| `10DE:2489` | `1462:3973` | MSI GeForce RTX 3060 Ti 8GB Gaming X LHR |
| `10DE:2489` | `1462:3903` | MSI GeForce RTX 3060 Ti 8GB Gaming X Trio LHR |
| `10DE:2488` | `1462:3904` | MSI GeForce RTX 3070 8GB Gaming Trio |
| `10DE:2484` | `1462:3903` | MSI GeForce RTX 3070 8GB Gaming X Trio |
| `10DE:2488` | `1462:3901` | MSI GeForce RTX 3070 8GB Suprim X LHR |
| `10DE:2482` | `1462:5052` | MSI GeForce RTX 3070 Ti 8GB Gaming X Trio |
| `10DE:2482` | `1462:5051` | MSI GeForce RTX 3070 Ti Suprim X 8G |
| `10DE:2206` | `1462:389B` | MSI GeForce RTX 3080 10GB Gaming Z Trio |
| `10DE:2216` | `1462:389B` | MSI GeForce RTX 3080 10GB Gaming Z Trio LHR |
| `10DE:2206` | `1462:3892` | MSI GeForce RTX 3080 10GB Gaming X Trio |
| `10DE:2206` | `1462:3897` | MSI GeForce RTX 3080 Suprim X 10G |
| `10DE:2216` | `1462:3897` | MSI GeForce RTX 3080 Suprim X 10G LHR |
| `10DE:220A` | `1462:389B` | MSI GeForce RTX 3080 12GB Gaming Z Trio LHR |
| `10DE:2208` | `1462:389B` | MSI GeForce RTX 3080 Ti Gaming X Trio 12G |
| `10DE:2208` | `1462:3897` | MSI GeForce RTX 3080 Ti Suprim X 12G |
| `10DE:2204` | `1462:3884` | MSI GeForce RTX 3090 24GB Gaming X Trio |
| `10DE:2204` | `1462:3882` | MSI GeForce RTX 3090 Suprim X 24G |
| `1002:73BF` | `1462:3953` | MSI Radeon RX 6800 XT Gaming Z Trio |
