# MSI Mystic Light (185 Byte)

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1462` | `7B93` | MSI Mystic Light MS_7B93 |
| `1462` | `7C34` | MSI Mystic Light MS_7C34 |
| `1462` | `7C35` | MSI Mystic Light MS_7C35 |
| `1462` | `7C37` | MSI Mystic Light MS_7C37 |
| `1462` | `7C56` | MSI Mystic Light MS_7C56 |
| `1462` | `7C59` | MSI Mystic Light MS_7C59 |
| `1462` | `7C71` | MSI Mystic Light MS_7C71 |
| `1462` | `7C75` | MSI Mystic Light MS_7C75 |
| `1462` | `7C76` | MSI Mystic Light MS_7C76 |
| `1462` | `7C79` | MSI Mystic Light MS_7C79 |
| `1462` | `7C80` | MSI Mystic Light MS_7C80 |
| `1462` | `7C81` | MSI Mystic Light MS_7C81 |
| `1462` | `7C83` | MSI Mystic Light MS_7C83 |
| `1462` | `7C84` | MSI Mystic Light MS_7C84 |
| `1462` | `7C86` | MSI Mystic Light MS_7C86 |
| `1462` | `7C90` | MSI Mystic Light MS_7C90 |
| `1462` | `7C91` | MSI Mystic Light MS_7C91 |
| `1462` | `7C92` | MSI Mystic Light MS_7C92 |
| `1462` | `7C94` | MSI Mystic Light MS_7C94 |
| `1462` | `7C95` | MSI Mystic Light MS_7C95 |
| `1462` | `7D06` | MSI Mystic Light MS_7D06 |
| `1462` | `7D07` | MSI Mystic Light MS_7D07 |
| `1462` | `7D09` | MSI Mystic Light MS_7D09 |
| `1462` | `7D13` | MSI Mystic Light MS_7D13 |
| `1462` | `7D15` | MSI Mystic Light MS_7D15 |
| `1462` | `7D25` | MSI Mystic Light MS_7D25 |
| `1462` | `7D29` | MSI Mystic Light MS_7D29 |
| `1462` | `7D54` | MSI Mystic Light MS_7D54 |
| `1462` | `7D31` | MSI Mystic Light MS_7D31 |
