# PNY GPU

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:2204` | `196E:136A` | PNY XLR8 Revel EPIC-X RTX 3090 |
| `10DE:2503` | `1569:2503` | Palit 3060 |
| `10DE:2504` | `1569:2504` | Palit 3060 LHR |
| `10DE:2486` | `1569:2486` | Palit 3060Ti |
| `10DE:2489` | `1569:2489` | Palit 3060Ti |
| `10DE:2484` | `1569:2484` | Palit 3070 |
| `10DE:2488` | `1569:2488` | Palit 3070 LHR |
| `10DE:2482` | `1569:2482` | Palit 3070Ti GamingPro |
| `10DE:2482` | `1569:F278` | Palit 3070Ti |
| `10DE:2206` | `1569:2206` | Palit 3080 |
| `10DE:2216` | `1569:2216` | Palit 3080 LHR |
| `10DE:2208` | `1569:2208` | Palit 3080Ti |
| `10DE:2204` | `1569:2204` | Palit 3090 |
| `10DE:2486` | `10DE:2486` | Palit GeForce RTX 3060 Ti Dual |
| `10DE:2489` | `10DE:2489` | Palit GeForce RTX 3060 Ti Dual OC |
| `10DE:1E81` | `10DE:1E81` | NVIDIA RTX2080S |
