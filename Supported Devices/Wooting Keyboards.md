# Wooting Keyboards

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `03EB` | `FF01` | Wooting ONE Keyboard |
| `03EB` | `FF02` | Wooting TWO Keyboard |
| `31E3` | `1210` | Wooting TWO Keyboard LE |
| `31E3` | `1220` | Wooting TWO Keyboard HE |
