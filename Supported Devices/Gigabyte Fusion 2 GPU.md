# Gigabyte Fusion 2 GPU

 

## Connection Type
 I2C

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1F06` | `1458:3FF7` | Gigabyte AORUS RTX2060 SUPER 8G V1 |
| `10DE:1F07` | `1458:37B4` | Gigabyte AORUS RTX2070 XTREME 8G |
| `10DE:1E84` | `1458:3FF6` | Gigabyte AORUS RTX2070 SUPER 8G |
| `10DE:1E84` | `1458:3FF5` | Gigabyte AORUS RTX2070 SUPER 8G |
| `10DE:1E87` | `1458:37B2` | Gigabyte AORUS RTX2080 8G |
| `10DE:1E81` | `1458:3FF3` | Gigabyte AORUS RTX2080 SUPER 8G |
| `10DE:1E81` | `1458:4003` | Gigabyte AORUS RTX2080 SUPER Waterforce WB 8G |
| `10DE:2504` | `1458:407B` | Gigabyte AORUS RTX3060 ELITE 12G |
| `10DE:2489` | `1458:4076` | Gigabyte AORUS RTX3060 Ti ELITE 8G LHR |
| `10DE:2486` | `1458:405E` | Gigabyte RTX3060 Ti GAMING OC PRO 8G |
| `10DE:2489` | `1458:405E` | Gigabyte RTX3060 Ti Gaming OC PRO 8G LHR |
| `10DE:2484` | `1458:4069` | Gigabyte RTX3070 MASTER 8G |
| `10DE:2206` | `1458:4038` | Gigabyte AORUS RTX3080 XTREME WATERFORCE WB 10G |
| `10DE:2216` | `1458:4038` | Gigabyte AORUS RTX3080 XTREME WATERFORCE WB 10G |
| `10DE:2216` | `1458:4037` | Gigabyte AORUS RTX3080 XTREME WATERFORCE 10G Rev 2.0 |
| `10DE:2208` | `1458:4087` | Gigabyte RTX3080 Ti Vision OC 12G |
| `10DE:2208` | `1458:4083` | Gigabyte AORUS RTX3080 Ti XTREME WATERFORCE 12G |
| `10DE:2204` | `1458:403A` | Gigabyte AORUS RTX3090 XTREME WATERFORCE WB 24G |
