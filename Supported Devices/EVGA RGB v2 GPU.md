# EVGA RGB v2 GPU

 

## Connection Type
 I2C

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1F07` | `3842:2172` | EVGA GeForce RTX 2070 XC Gaming |
| `10DE:1F07` | `3842:2173` | EVGA GeForce RTX 2070 XC OC |
| `10DE:1E84` | `3842:3173` | EVGA GeForce RTX 2070 SUPER XC Ultra |
| `10DE:1E84` | `3842:3175` | EVGA GeForce RTX 2070 SUPER XC Ultra+ |
| `10DE:1E84` | `3842:3277` | EVGA GeForce RTX 2070 SUPER FTW3 Ultra |
| `10DE:1E84` | `3842:3377` | EVGA GeForce RTX 2070 SUPER FTW3 Ultra+ |
| `10DE:1E82` | `3842:2081` | EVGA GeForce RTX 2080 Black |
| `10DE:1E87` | `3842:2082` | EVGA GeForce RTX 2080 XC Black |
| `10DE:1E87` | `3842:2182` | EVGA GeForce RTX 2080 XC Gaming |
| `10DE:1E87` | `3842:2183` | EVGA GeForce RTX 2080 XC Ultra Gaming |
| `10DE:1E81` | `3842:3182` | EVGA GeForce RTX 2080 SUPER XC Gaming |
| `10DE:1E81` | `3842:3183` | EVGA GeForce RTX 2080 SUPER XC Ultra |
| `10DE:1E81` | `3842:3287` | EVGA GeForce RTX 2080 SUPER FTW3 Ultra |
| `10DE:1E81` | `3842:3288` | EVGA GeForce RTX 2080 SUPER FTW3 Hybrid OC |
| `10DE:1E81` | `3842:3289` | EVGA GeForce RTX 2080 SUPER FTW3 Ultra Hydro Copper |
| `10DE:1E04` | `3842:2281` | EVGA GeForce RTX 2080Ti Black |
| `10DE:1E07` | `3842:2383` | EVGA GeForce RTX 2080Ti XC Ultra |
| `10DE:1E07` | `3842:2384` | EVGA GeForce RTX 2080Ti XC HYBRID GAMING |
| `10DE:1E07` | `3842:2487` | EVGA GeForce RTX 2080Ti FTW3 Ultra |
