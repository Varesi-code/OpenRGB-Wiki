# Anne Pro 2

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `04D9` | `8008` | Anne Pro 2 |
| `04D9` | `8009` | Anne Pro 2 |
| `04D9` | `A292` | Anne Pro 2 |
| `04D9` | `A293` | Anne Pro 2 |
